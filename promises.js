function echo(text, err){
	return new Promise(function(resolve, reject){

		setTimeout(function(){
            err? reject('Upss!') : resolve(text+' ')
        },2000)

    })
}
// DataService
var r = echo("Alice", true)

r = r.catch(function(err){ return 'Nikt nie ' })

// BusinessService
var r2 = r.then(function(resp){ return echo(resp + ' ma ') } )


// ApplicationService
var r3 = r2.then(function(resp){ return echo(resp + ' kota ') } )

// AppController
r3.then(function(resp){ console.log(resp) }, function(resp){ console.log('Error ' +resp) })
