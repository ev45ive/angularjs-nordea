angular.module('todos')
    .controller('todosCtrl',
        function ($scope) {
            // var todos = []
            // $scope.todos = todos;


            $scope.todos = [
                {
                    title: "Learn Angular",
                    completed: false,
                },
                {
                    title: "Placki",
                    completed: true
                }
            ]

            $scope.todo = {
                "title": ""
            }

            $scope.addTodo = function (data) {
                var todo = angular.copy(data)
                todo.id = Date.now()

                $scope.todos.push(todo)

                // Clear todo
                $scope.todo = {
                    title: ''
                }
            }

            $scope.removeTodo = function (todo) {
                var index = $scope.todos.indexOf(todo)
                $scope.todos.splice(index, 1)
            }

            $scope.enterTodo = function (event) {
                if (event.keyCode !== 13) {
                    return
                }
                $scope.addTodo($scope.todo)
            }

            $scope.archived = []
            $scope.archiveCompleted = function(){
                var completed = $scope.todos.filter(function(todo){
                    return todo.completed
                })
                $scope.archived = $scope.archived.concat(completed)
                $scope.todos = $scope.todos.filter(function(todo){
                    return !todo.completed
                })

                // ==== or ==== 
/* 
                var groups = $scope.todos.reduce( function( group, todo){
                    if(todo.completed){
                        group.archived.push(todo)
                    }else{
                        group.todos.push(todo)
                    }
                    return group;
                },{
                    archived:[], todos:[]
                })
                $scope.archived = $scope.archived.concat(groups.archived)
                $scope.todos = groups.todos */

            }

            // {{ getCompletedCount() }}
            $scope.getCompletedCount = function(){
                // console.log('Calculating ...')

                return $scope.todos.filter(function(todo){
                    return todo.completed
                }).length
            }
        })