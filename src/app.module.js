// Create module
angular.module('my-app', [
    'todos',
    'directives',
    'users'
])

/// =============

.provider('ConfigService', function(){

    var serverURL = ''

    return {
        setServerURL: function(url){
            serverURL = url
        },
        $get: function(){
            return {
                getServerURL(){
                    return serverURL
                }
            }
        }
    }
})

.config(function(ConfigServiceProvider){
    ConfigServiceProvider.setServerURL('placki')
})

.run(function(ConfigService, $rootScope){
    // console.log(ConfigService.getServerURL())

    $rootScope.date = (new Date()).toLocaleDateString()
})