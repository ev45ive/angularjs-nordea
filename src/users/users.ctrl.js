angular.module('users')
    .controller('usersCtrl',
        function ($scope, Users) {

            Users.fetchUsers()
                .then(function (users) {
                    $scope.users = users
                })
            
            // $scope.service = Users
            // $scope.$watch('service.users',...

            $scope.$watch(function(){
                return Users.users
            },function(users){
                $scope.users = users
            })

            /* 
                Users.getUsersStream()
                .subscribe(users => 
                    this.users = users
                )
            */

            $scope.users = [
                {
                    name: "Test User",
                    email: "test@user.com",
                },
                {
                    name: "Another User ",
                    email: "Another@user.com",
                },
            ]

            $scope.selected = $scope.users[0]

            $scope.selectUser = function (user) {
                $scope.selected = user
            }
        })


