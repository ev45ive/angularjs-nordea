angular.module('todos')
    .controller('TodosListCtrl',
        function (Todos) {
            console.log(Todos)

            /* this.todos = [
                {
                    title: 'Test todo'
                }
            ] */
            this.todos = Todos.getTodos()

            this.todo = {
                "title": ""
            }

            this.addTodo = function (data) {

                var todo = angular.copy(data)
                todo.id = Date.now()

                this.todos.push(todo)

                // Clear todo
                this.todo = {
                    title: ''
                }
            }


            this.enterTodo = function ($event) {

                if (event.keyCode !== 13) {
                    return
                }
                this.addTodo(this.todo)
            }

            this.removeTodo = function (todo) {
                var index = this.todos.indexOf(todo)
                this.todos.splice(index, 1)
            }

            /* this.clearTodos = function(){
                // this.todos = []
                Todos.clearTodos()
                // this.todos = Todos.getTodos()
            } */


            this.getCompletedCount = function () {

                return this.todos.filter(function (todo) {
                    return todo.completed
                }).length
            }
        }



    )

// ng-controller="TodosListCtrl as todosList"
// $scope.todosList = TodosListCtrl