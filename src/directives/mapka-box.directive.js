// E -  <mapka-box><mapka-box>
// A -  <div mapka-box><div>
// C -  <div class="mapka-box" ><div>
// M -  <!-- mapka-box --> 

angular.module('directives')
    .directive('mapkaBox', function () {

        function makeUrl(lat, lng, zoom) {
            return 'https://maps.googleapis.com/maps/api/staticmap'
                + '?center=' + lat + ',' + lng
                + '&size=300x300'
                + '&zoom=' + zoom
                + '&key=AIzaSyAnKFH-tmY9wGKt8hjK0X3lWK0cKGigr7s'
        }

        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                // console.log(scope, element, attrs)

                element.append('<img>')
                var img = element.find('img')
                
                /// Integrating with jQuery, etc.
                /* $(img).datepicker()

                scope.$watch('date', datePicker.updateDate)

                datePicker.on('change', function(date){
                    scope.date = date
                    scope.$digest()
                }) */
                ///

                img.on('click', function(event){    
  
                    // scope.$apply(function(){

                        scope.$evalAsync(attrs.onSelect,{
                            position: 'Placki!' + Date.now()
                        })
                    // })

                    // scope.$digest()
                })

                var lat = scope.$eval(attrs.lat)
                var lng = scope.$eval(attrs.lng)
                var zoom = scope.$eval(attrs.zoom)

                scope.$watch(attrs.lat, function (newValue) {
                    lat = newValue
                    updateMap(lat, lng, zoom)
                })

                scope.$watch(attrs.lng, function (newValue) {
                    lng = newValue
                    updateMap(lat, lng, zoom)
                })

                scope.$watch(attrs.zoom, function (newValue) {
                    zoom = newValue
                    updateMap(lat, lng, zoom)
                })

                /*  scope.$watchGroup([attrs.lat,attrs.lng], function(newVal){
                     updateMap(newVal[0],newVal[1])
                 })*/

                function updateMap(lat, lng, zoom) {
                    var url = makeUrl(lat, lng, zoom)
                    img.attr('src', url)
                }
            }
        }
    })