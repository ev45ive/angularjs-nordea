angular.module('users')
    /*
    <user-card user="someUser" onSave="doSmth(user)" 
     */
    .directive('userCard', function () {
        return {
            scope: {
                user: '=',
            },
            transclude: true,
            templateUrl: 'src/users/user-card.tpl.html'
        }
    })

    // <users-list users=" [] "></users-list>
    .directive('usersList', function () {
        return {
            scope: {
                selected: '=',
                users: '=', //'=users',
                onSelect: '&'
            },
            templateUrl: 'src/users/users-list.tpl.html'

        }
    })

    // <user-details user="someUser"><user-details>
    .directive('userDetails', function () {
        return {
            scope: {
                user: '='
            },
            templateUrl: 'src/users/user-details.tpl.html',
            controller: function($scope, Users){
                $scope.mode = 'show'

                $scope.edit = function(){
                    $scope.draft = angular.copy($scope.user)
                    $scope.mode = 'edit'
                }

                $scope.show = function(){
                    $scope.mode = 'show'
                }

                $scope.saveUser = function(user){

                    Users.saveUser(user)
                    .then(function(user){
                        $scope.user = user
                        $scope.show()
                    })
                    
                }
            },
            // controllerAs:"$ctrl"
        }
    })

    .directive('userForm', function(){
        
        return {
            scope:{
                user:'=',
                save:'&'
            },
            transclude:true,
            templateUrl:'src/users/user-form.tpl.html',
            controller: function(){


            },
            // controllerAs:'$ctrl'
        }
    })