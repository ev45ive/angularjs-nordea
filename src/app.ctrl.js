angular.module('my-app')
.controller('appCtrl', function ($scope) {

    $scope.user = {
        name: ''
    }
    
    $scope.currentPage = 'src/users/users.tpl.html'

    $scope.pages = [
        {
            name:'Todos',
            path: 'src/todos/todos.tpl.html'
        },
        {
            name:'Directives',
            path: 'src/directives/directives.tpl.html'
        },
        {
            name:'Users',
            path: 'src/users/users.tpl.html'
        },
    ]
    

    $scope.navigateTo = function(page){
        $scope.currentPage = page.path
    }
})