angular.module('users')
    .service('Users', ['$http', function ($http) {

        this.users = []

        this.getUsers = function(){
            return this.users
        }

        this.fetchUsers = function () {
            var that = this;

            return $http
                .get('http://localhost:9000/users')
                .then(function (resp) {
                    return resp.data
                })
                .then(function(users){
                    that.users = users;
                    return users
                }.bind(this))
        }

        this.saveUser = function (user) {
            return $http.put('http://localhost:9000/users/'+user.id,user)
                .then(function(resp){
                    this.fetchUsers()

                    return resp.data
                }.bind(this))
        }


        /* 
            usersStream.next(users)
        */


    }])

   /*  .provider('',function AServiceProvider(){
        return {
            $get: function AService(){
                this.fetchUsers = function(){}
            }
        }
    }) */


// json-server https:// jsonplaceholder.typicode.com/db

// S + ENTER

// npm i json-server --save-dev
// json-server .\db-654636474307273498573407238728762876.json

// package.json => "scripts":{
//                  ... "api":"json-server --port 9000 --watch ./db-1530092182141.json"

// npm run api

/* 
class MojController{

    @Autowired
    MojController(ServiceA a){

    }
} */

/* angular.module('users')
.service('MojController',MojController)
MojController.$inject = ['$scope','ServiceA']
function MojController(s,a){

} */

/* angular.module('users')
.service('MojController',MojController)
MojController.$inject = ['$scope','ServiceA']
class MojController{
    constructor(s,a){

    }
} */

/* // Angular 2
@Controller()
class MojController{
    constructor(a:ServiceA){

    }
} */