angular.module('todos')
.service('Todos', function(ConfigService){
    
    this.todos = [
        {
            title:"From Service"
        }
    ]

    // this.subscribeTodosChange = function(){}

    this.addTodo = function(todo){
        this.todos.push(todo)
    }

    this.getTodos = function(){
        return this.todos
    }
})